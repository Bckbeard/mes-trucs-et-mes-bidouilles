# Mes trucs et mes bidouilles

Une séries d’articles sur mes découvertes, recherches, trucs et bidouilles en 3d et en rig plus particulièrement.
L’idée, c’est de partager mes hacks du quotidien, ma popote interne, mes trucs débiles...
Un espace qui va évoluer, les articles et les fichiers à dispo.
Sentez-vous libre de cloner, récupérer, corriger, amiliorer.

A series of articles on my discoveries, research, tips and tricks in 3d and especially in rig.
The idea is to share my everyday hacks, my internal cooking, my stupid things...
A space that will evolve with articles and files available.
Feel free to clone, recover, fix, improve.

## Installation

Pas d'installation particulière.

No installation needed

## Authors and acknowledgment

Me for the moment.

## License

[MIT](https://choosealicense.com/licenses/mit/)